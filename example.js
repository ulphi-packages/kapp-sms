/**
 * @description Send Domestic SMS 
 * @param {*} number 
 * @param {*} text 
 */
var sendIndianSMS = function (number, text) {
    console.log('Send Test SMS');
    var msgBody = {
        "authentication": {
            "username": "mobilean",
            "password": "k21sqC3s"
        },
        "messages": {
            "sender": "ILNINM",
            "text": text,
            "recipients": [{
                "gsm": number
            }]

        }
    };
    var reqOptions = {
        url: 'http://193.105.74.159/api/v3/sendsms/json',
        method: 'POST',
        json: true,
        body: msgBody
    };
    console.dir('Req Options:', reqOptions);
    request(reqOptions, function (err, httpResponse, body) {
        console.log('SMS Response', body);
        return ({
            status: false,
            err: err,
            httpResponse: httpResponse,
            body: body
        });
    });
};
var sendInternationalSMS = function (number, text) {
    console.log('Send Test SMS');
    var msgBody = {
        "authentication": {
            "username": "ilove9months",
            "password": "WXUhF905"
        },
        "messages": {
            "sender": "ILNINM",
            "text": text,
            "recipients": [{
                "gsm": number
            }]

        }
    };
    var reqOptions = {
        url: 'http://193.105.74.159/api/v3/sendsms/json',
        method: 'POST',
        json: true,
        body: msgBody
    };
    console.dir('Req Options:', reqOptions);
    console.log('Sending International SMS to', msgBody.messages.recipients[0].gsm);
    request(reqOptions, function (err, httpResponse, body) {
        console.log('International SMS status:');
        console.dir('International SMS Error status:', err);
        console.dir('International SMS httpResponse status:', httpResponse);
        console.dir('International SMS body status:', body);

        return ({
            status: false,
            err: err,
            httpResponse: httpResponse,
            body: body
        });
    });
};