# Description
Use this package as a wrapper for sending SMS using kap systems. 
[Kap Systems](http://instant.kapsystem.com)
----------
# Usage
You need to pass a config object with the following information:

```

{
    "authentication": {
        "username": "", 
        "password": ""
    },
    "messages": {
        "sender": "", //Sender Id
        "text": "test", //SMS Content
        "recipients": [{
            "gsm": 0
        }] //Numbers

    }
}

```

1. Authentication
  1. username: kap username 
  2. password: kap password
2. Messages
  1. Sender: This is the 6 letter sender id, it needs to be in capital letters and exactly 6 cahracters
  2. text: The SMS content
  3. recipents: An array consisting of phone numbers. 
    1. Example: 

``` 

{ gsm: 919876543210} 

``` 

please include first two digits of country code. 

## Example

```

var kappSms = require('kap-sms');
/**
 * Please give your username and password along with phone number
 */
var msgBody = {
    "authentication": {
        "username": "demo",
        "password": "demouser"
    },
    "messages": {
        "sender": "TESTKA",
        "text": "test",
        "recipients": [{
            "gsm": 910123456789
        }]

    }
};
kappSms.sendSMS(msgBody).then(function (success) {
    console.log('Successfully Sent the SMS');
    console.dir(success.results);
}, function (error) {
    console.log('Problem Sending the SMS');
    console.dir(error);
});

```
The function call returns a promise with success and error responses.