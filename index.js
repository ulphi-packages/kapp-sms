// Package to make http calls 
var request = require('request');

var validateConfig = function (config) {
    var respnseBody = {
        status: false,
        message: ''
    }
    if (config.authentication) {
        if (config.authentication.username.length > 0 && config.authentication.password.length > 0) {
            if (config.messages.sender.length > 0 && config.messages.text.length > 0 && config.messages.recipients.length > 0) {
                respnseBody.message = "Valid";
                respnseBody.status = true;
                return respnseBody;
            } else {
                respnseBody.message = "Please Check Message, One or More fields are missing";
                respnseBody.status = false;
                return respnseBody;
            }
        } else {
            respnseBody.message = "Please Check Authentication, One or More fields are missing";
            respnseBody.status = false;
            return respnseBody;
        }
    } else {
        respnseBody.message = "Please Check Message, One or More fields are missing";
        respnseBody.status = false;
        return respnseBody;
    }
};

var sendSMS = function (config) {
    return new Promise(function (fulfill, reject) {
        var validBody = validateConfig(config);
        if (validBody.status) {
            var reqOptions = {
                url: 'http://193.105.74.159/api/v3/sendsms/json',
                method: 'POST',
                json: true,
                body: msgBody
            };
            request(reqOptions, function (err, httpResponse, body) {
                // console.log('SMS Response', body);
                if (err) {
                    reject ({
                        status: false,
                        err: err,
                        httpResponse: httpResponse,
                        body: body
                    });
                } else {
                    fulfill ({
                        status: true,
                        results: body.results,
                        message: 'Sent SMS'
                    });
                }

            });
            
        } else {
            reject(validBody);
        }
    });
}

exports.sendSMS = sendSMS;