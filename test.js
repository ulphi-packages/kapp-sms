var kappSms = require('./index.js');
/**
 * Please give your username and password along with phone number
 */
var msgBody = {
    "authentication": {
        "username": "",
        "password": ""
    },
    "messages": {
        "sender": "TESTKA",
        "text": "test",
        "recipients": [{
            "gsm": 0
        }]

    }
};
kappSms.sendSMS(msgBody).then(function (success) {
    console.log('Successfully Sent the SMS');
    console.dir(success.results);
}, function (error) {
    console.log('Problem Sending the SMS');
    console.dir(error);
});